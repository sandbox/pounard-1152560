<?php
// $Id$

/**
 * @file
 * View Composer pages.
 */

function vc_page_bundle_page($oid) {
  $build = array();
  $bundle = xoxo_storage_get('vc_bundle')->loadByIdentifier($oid);
  $build['#theme'] = 'vc_bundle';
  $build['#bundle'] = $bundle;
  return $build;
}
