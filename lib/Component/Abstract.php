<?php

class Vc_Component_Abstract
  extends Oox_Registrable_Abstract
  implements Schema_Exportable_Interface
{
  /**
   * @var Schema_Interface
   */
  protected $_config;

  /**
   * @see Schema_Exportable_Interface::getSchema()
   */
  public function getSchema() {
    return clone $this->_config;
  }

  /**
   * @see Schema_Exportable_Interface::restoreFromSchema()
   */
  public function restoreFromSchema(Schema_Interface $config) {
    $this->_config->merge($config, TRUE);
  }

  /**
   * Default constructor.
   */
  public function __construct() {
    $this->_config = new Schema_Array(array(), FALSE, FALSE);
  }
}