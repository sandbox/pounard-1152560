<?php

abstract class Vc_Component_Dummy
  extends Oox_Registrable_Dummy
  implements Schema_Exportable_Interface, Oox_NullObject_Interface
{
  /**
   * @see Schema_Exportable_Interface::getSchema()
   */
  public function getSchema() {
    return Oox_NullObject::getInstance('Schema_Dummy');
  }

  /**
   * @see Schema_Exportable_Interface::restoreFromSchema()
   */
  public function restoreFromSchema(Schema_Interface $config) {}

  /**
   * Default constructor.
   */
  public function __construct() {}
}