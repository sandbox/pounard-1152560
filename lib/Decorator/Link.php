<?php

/**
 * Arbitrary link decorator.
 */
class Vc_Decorator_Link
  extends Vc_Component_Abstract
  implements Vc_Decorator_Interface, Formable_Interface
{
  /**
   * Set link target.
   * 
   * @param string $target
   */
  public function setTarget($target) {
    return $this->_config->set('target', $target);
  }

  /**
   * Get link target.
   * 
   * @return string
   */
  public function getTarget() {
    return $this->_config->get('target');
  }

  /**
   * Set link label.
   * 
   * @param string $label
   */
  public function setLabel($label) {
    $this->_config->set('label', $label);
  }

  /**
   * Get link label.
   * 
   * @return string
   */
  public function getLabel() {
    return $this->_config->get('label');
  }

  /**
   * Get l() function options array.
   * 
   * @return array
   */
  public function getLinkOptions() {
    // FIXME: Cannot store an array.
    /*
    $options = $this->_config->get('link_options');
    if (isset($options)) {
      return $options;
    }
    return array();
     */
  }

  /**
   * @see Vc_Decorator_Interface::canDecorate()
   */
  public function canDecorate(Vc_Bundle_Interface $bundle) {
    return TRUE;
  }

  /**
   * @see Vc_Decorator_Interface::decorate()
   */
  public function preQuery(Vc_Bundle_Interface $bundle) {
    return $this;
  }

  /**
   * @see Vc_Decorator_Interface::decorate()
   */
  public function decorate(Vc_Bundle_Interface $bundle, array &$build) {
    if (($label = $this->getLabel()) && ($target = $this->getTarget())) {
      $build['link']['#markup'] = l($label, $target, $this->getLinkOptions());
    }
    return $this;
  }

  /**
   * @see Formable_Interface::getForm()
   */
  public function getForm($formIdentifier = Formable_Interface::FORM_DEFAULT) {
    return new Vc_Decorator_LinkFormDefault;
  }

  /**
   * @see Formable_Interface::getFormIdentifiers()
   */
  public function getFormIdentifiers() { return array(); }
}
