<?php

/**
 * Vc_Decorator_Link form helper.
 */
class Vc_Decorator_LinkFormDefault extends Formable_Form_Abstract
{
  /**
   * @see Oox_Formable_Interface::form()
   */
  public function formBuild(array &$form, array $values = array(), $parent = NULL) {
    $form['target'] = array(
      '#type' => 'textfield',
      '#title' => t("Link target"),
      '#default_value' => $parent->getTarget(),
      '#description' => t("Any kind of link, may be an Drupal internal path or an external link."),
    );
    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => t("Link label"),
      '#default_value' => $parent->getLabel(),
      '#description' => t("Any kind of content is allowed here, it will be filtered when outputed."),
    );
  }

  /**
   * @see Oox_Formable_Interface::formValidate()
   */
  public function formValidate(array &$values, $parent = NULL) {
    $errors = array();
    if (empty($values['target'])) {
      $errors['target'] = t("Target cannot be empty.");
    }
    if (empty($values['label'])) {
      $errors['label'] = t("Label cannot be empty.");
    }
    return $errors;
  }

  /**
   * @see Oox_Formable_Interface::formSubmit()
   */
  public function formSubmit(array &$values, $parent = NULL) {
    $parent->setTarget($values['target']);
    $parent->setLabel($values['label']);
  }
}
