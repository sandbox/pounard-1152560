<?php

/**
 * Pager decorator.
 */
class Vc_Decorator_Pager
  extends Vc_Component_Abstract
  implements Vc_Decorator_Interface, Formable_Interface
{
  /**
   * Set pager element.
   * 
   * @param int $element
   * 
   * @return Vc_Decorator_Pager
   *   Self reference for chaining.
   */
  public function setPagerElement($pagerElement) {
    $this->_config->set('pager_element', $pagerElement);
    return $this;
  }

  /**
   * Get pager element.
   * 
   * @return int
   */
  public function getPagerElement() {
    $pagerElement = $this->_config->get('pager_element');
    if (!isset($pagerElement)) {
      // Get a default element dynamically using what's already been rendered.
      $this->_config->set('pager_element', $pagerElement = PagerDefault::$maxElement++);
    }
    $pagerElement;
  }

  /**
   * Set pager limit.
   * 
   * @param int $limit
   * 
   * @return Vc_Decorator_Pager
   *   Self reference for chaining.
   */
  public function setLimit($limit) {
    $this->_config->set('limit', $limit);
    return $this;
  }

  /**
   * Get pager limit.
   * 
   * @return int
   */
  public function getLimit() {
    $limit = $this->_config->get('limit');
    if (isset($limit)) {
      return $limit;
    }
    // Default value for element is 0 if not set (Drupal core default).
    // Leave a default limit of 10, this is totally arbitrary.
    return 10;
  }

  /**
   * @see Vc_Decorator_Interface::canDecorate()
   */
  public function canDecorate(Vc_Bundle_Interface $bundle) {
    return $bundle->getObjectStream() instanceof Oox_ObjectStream_Browsable_Interface;
  }

  /**
   * @see Vc_Decorator_Interface::decorate()
   */
  public function preQuery(Vc_Bundle_Interface $bundle) {
    if (($objectStream = $bundle->getObjectStream()) instanceof Oox_ObjectStream_Browsable_Interface) {
      $limit = $this->getLimit();
      // Use pager with its low level API.
      $current_page = pager_default_initialize($objectStream->count(), $limit, $this->getPagerElement());
      $objectStream->setLimit($limit)->setOffset($current_page * $limit);
    }
    return $this;
  }

  /**
   * @see Vc_Decorator_Interface::decorate()
   */
  public function decorate(Vc_Bundle_Interface $bundle, array &$build) {
    if (($objectStream = $bundle->getObjectStream()) instanceof Oox_ObjectStream_Browsable_Interface) {
      $build['pager']['#markup'] = theme('pager', array(
        'element' => $this->getPagerElement(),
        'quantity' => 3,
        'tags' => array(
          // Be careful when copy/paste, blank space in strings are non breakable
          // white space characters.
          0 => ' « ',
          1 => ' ‹ ',
          3 => ' › ',
          4 => ' » ',
        ),
      ));
    }
    return $this;
  }

  /**
   * @see Formable_Interface::getForm()
   */
  public function getForm($formIdentifier = Formable_Interface::FORM_DEFAULT) {
    return new Vc_Decorator_PagerFormDefault;
  }

  /**
   * @see Formable_Interface::getFormIdentifiers()
   */
  public function getFormIdentifiers() { return array(); }
}
