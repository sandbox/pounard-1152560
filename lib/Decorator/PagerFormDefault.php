<?php

/**
 * Vc_Decorator_Pager form helper.
 */
class Vc_Decorator_PagerFormDefault extends Formable_Form_Abstract
{
  /**
   * @see Formable_Form_Interface::formBuild()
   */
  public function formBuild(array &$form, array $values = array(), $parent = NULL) {
    $form['pager_element'] = array(
      '#type' => 'textfield',
      '#title' => t("Pager element"),
      '#default_value' => $parent->getPagerElement(),
      '#description' => t("Fixing a number here could avoid conflicts with other pager in some edge cases. In most case, leave empty and the pager element will be determined automatically depending on what is being displayed on same page."),
    );
  }

  /**
   * @see Formable_Form_Interface::formValidate()
   */
  public function formValidate(array &$values, $parent = NULL) {
    $errors = array();
    if (!empty($values['pager_element'])) {
      if (!preg_match('/^\d+$/', $values['pager_element'])) {
        $errors['pager_element'] = t("Pager element must be empty or a positive integer.");
      }
    }
    return $errors;
  }

  /**
   * @see Formable_Form_Interface::formSubmit()
   */
  public function formSubmit(array &$values, $parent = NULL) {
    $parent->setPagerElement(empty($values['pager_element']) ? NULL : $values['pager_element']);
  }
}
