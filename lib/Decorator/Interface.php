<?php

/**
 * A bundle decorator represent an additional feature, such as pager, than can
 * decorate a bundle display.
 * 
 * Decorators can apply under certain circumstances. For example, a pager
 * decorator can apply only if the underlaying request is a browsable request.
 * 
 * We could also imagine that an exposed filter form might be a bundle
 * decorator. The various arguments and parameters will be tied to the bundle
 * itself not to the frontend or the request.
 * 
 * Decorators belong to the bundle, not to the frontend.
 * 
 * FIXME: I'm not proud of this interface, it does not reflect the real
 * decorator pattern I would have wanted here.
 * 
 * Use the Schema_Exportable_Interface for a full export, this will ensure
 * your objects are being really saved.
 */
interface Vc_Decorator_Interface extends Vc_Component_Interface
{
  /**
   * Tells if the current implementation can be used with the given couple.
   * 
   * @param Vc_Bundle_Interface $bundle
   *   Bundle instance.
   * 
   * @return boolean
   */
  public function canDecorate(Vc_Bundle_Interface $bundle);

  /**
   * Give a chance for decorator to alter the backend before query has been
   * done.
   * 
   * @param Vc_Bundle_Interface $bundle
   * 
   * @return Vc_Decorator_Interface
   *   Self reference for chaining.
   */
  public function preQuery(Vc_Bundle_Interface $bundle);

  /**
   * Give a chance for decorator to alter the backend before query has been
   * done.
   * 
   * @param Vc_Bundle_Interface $bundle
   *   Already rendered bundle.
   * @param array $build
   *   Array ready for render() method, containing rendered bundle and
   *   already decorated data from previous decorators.
   * 
   * @return Vc_Decorator_Interface
   *   Self reference for chaining.
   */
  public function decorate(Vc_Bundle_Interface $bundle, array &$build);
}
