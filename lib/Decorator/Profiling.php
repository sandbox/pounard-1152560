<?php

/**
 * Profiling decorator.
 */
class Vc_Decorator_Profiling
  extends Vc_Component_Abstract
  implements Vc_Decorator_Interface
{
  /**
   * @var int
   */
  protected $_memoryStart;

  /**
   * @var float
   */
  protected $_timeStart;

  /**
   * @see Vc_Decorator_Interface::canDecorate()
   */
  public function canDecorate(Vc_Bundle_Interface $bundle) {
    return TRUE;
  }

  /**
   * @see Vc_Decorator_Interface::decorate()
   */
  public function preQuery(Vc_Bundle_Interface $bundle) {
    $this->_memoryStart = memory_get_usage();
    $this->_timeStart = microtime(TRUE);
    return $this;
  }

  /**
   * @see Vc_Decorator_Interface::decorate()
   */
  public function decorate(Vc_Bundle_Interface $bundle, array &$build) {
    $memStop = memory_get_usage();
    $timeStop = microtime(TRUE);
    $m = round((($memStop - $this->_memoryStart) / 1024 / 1024), 2) . ' M';
    $t = round((($timeStop - $this->_timeStart) * 1000), 2) . ' ms';
    $build['profiling'] = array(
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => t("Generation took <strong>@time</strong> and <strong>@memory</strong>.", array(
        '@time' => $t,
        '@memory' => $m,
      )),
    );
    return $this;
  }
}