<?php

/**
 * Allows display of requests inside Drupal page.
 */
class Vc_Bundle_Page extends Vc_Bundle_Abstract implements Formable_Interface
{
  /**
   * Set path.
   * 
   * @param string $path
   *   Drupal menu path.
   */
  public function setPath($path) {
    $this->_config->set('path', $path);
  }

  /**
   * Get path.
   * 
   * @return string
   *   Drupal menu path.
   */
  public function getPath() {
    return $this->_config->get('path');
  }

  /**
   * Set page title.
   * 
   * @param string $title
   *   New page title.
   */
  public function setTitle($title) {
    $this->_config->set('title', $title);
  }

  /**
   * Get page title.
   * 
   * @param boolean $descriptionAsFallback = TRUE
   * 
   * @return string
   *   Current page title.
   */
  public function getTitle($descriptionAsFallback = TRUE) {
    $title = $this->_config->get('title');
    if (isset($title) && !empty($title)) {
      return $title;
    }
    else if ($descriptionAsFallback) {
      return $this->getDescription();
    }
  }

  /**
   * Change the localized state of the page content.
   */
  public function setLocalized($toggle) {
    $this->_config->set('localized', (bool) $toggle);
  }

  /**
   * Tell if the current bundle is localized.
   * 
   * @return boolean
   */
  public function isLocalized() {
    $localized = $this->_config->get('localized');
    return isset($localized) && $localized;
  }

  /**
   * @see Formable_Interface::getForm()
   */
  public function getForm($formIdentifier = Formable_Interface::FORM_DEFAULT) {
    if (Formable_Interface::FORM_DEFAULT == $formIdentifier) {
      return new Vc_Bundle_PageFormDefault();
    }
  }

  /**
   * @see Xoxo_Object::postSave()
   */
  public function postSave() {
    parent::postSave();
    if (!variable_get('menu_rebuild_needed')) {
      variable_set('menu_rebuild_needed', 1);
    }
  }
}
