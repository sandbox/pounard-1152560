<?php

/**
 * Specific implementation of Xoxo_Storage for vc backend objects.
 */
class Vc_Bundle_Storage extends Xoxo_Storage_Object_Exportable
{
  /**
   * @see Xoxo_Storage::_populate()
   */
  protected function _populate($row, Xoxo_Object $object) {
    parent::_populate($row, $object);
    if ($row->backend_oid) {
      $object->setObjectStream(xoxo_storage_get('vc_backend')->loadByIdentifier($row->backend_oid));
    }
  }

  /**
   * @see Xoxo_Storage::_preSave()
   */
  protected function _preSave(Xoxo_Object $object, &$data) {
    parent::_preSave($object, $data);
    if (($objectStream = $object->getObjectStream()) instanceof Vc_Backend_Persistent_Abstract) {
      $data['backend_oid'] = $objectStream->getIdentifier();
    }
  }
}
