<?php

/**
 * Allows display of requests inside Drupal blocks.
 */
class Vc_Bundle_Block extends Vc_Bundle_Abstract
{
  /**
   * Set page title.
   * 
   * @param string $title
   *   New page title.
   */
  public function setCacheMode($cacheMode) {
    $this->_config->set('cacheMode', $cacheMode);
  }

  /**
   * Get page title.
   * 
   * @return string
   *   Current page title.
   */
  public function getCacheMode() {
    if (isset($this->_options['cacheMode'])) {
      return $this->_options['cacheMode'];
    }
    else {
      return DRUPAL_CACHE_PER_USER;
    }
  }

  /**
   * Set page title.
   * 
   * @param string $title
   *   New page title.
   */
  public function setTitle($title) {
    $this->_config->set('title', $title);
  }

  /**
   * Get page title.
   * 
   * @return string
   *   Current page title.
   */
  public function getTitle($descriptionAsFallback = TRUE) {
    $title = $this->_config->get('title');
    if (isset($title) && !empty($title)) {
      return $title;
    }
    else if ($descriptionAsFallback) {
      return $this->getDescription();
    }
  }

  /**
   * @see Formable_Interface::getForm()
   */
  public function getForm($formIdentifier = Formable_Interface::FORM_DEFAULT) {
    if (Formable_Interface::FORM_DEFAULT == $formIdentifier) {
      return new Vc_Bundle_BlockFormDefault;
    }
  }
}
