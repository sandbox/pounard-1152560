<?php

/**
 * Vc_Bundle_Abstract form helper.
 */
class Vc_Bundle_AbstractFormDefault
  extends Xoxo_Object_ExportableFormDefault
{
  /**
   * @see Formable_Form_Interface::formBuild()
   */
  public function formBuild(array &$form, array $values = array(), $parent = NULL) {
    parent::formBuild($form, $values, $parent);

    $form['basics'] = array(
      '#type' => 'fieldset',
      '#title' => t("Basic options"),
      '#group' => 'components',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['xoxo']['status']['items'][] = t("Bundle using the <em>@implementation</em> implementation.", array(
      '@implementation' => t($parent->getRegistry()->getItemName($parent)),
    ));

    /*
     * FIXME:
     * 
     * Backend selection could be provided by a more generic component (backend
     * could be specifically switched in this form by the end user).
     */

    $objectStream = $parent->getObjectStream(FALSE);
    if (!isset($objectStream)) {
      $form['xoxo']['status']['items'][] = '<strong>' . t("No backend set.") . '</strong>';
    }
    else if ($objectStream instanceof Vc_Backend_Persistent_Abstract) {
      $form['xoxo']['status']['items'][] = t("Backend is <em>@name</em>.", array('@name' => $objectStream->getName())) . ' ' . l(t("You can edit it here."), $objectStream->getStorage()->getPath() . '/' . $objectStream->getName() . '/edit');
    }
    else if ($objectStream instanceof Vc_Backend_Abstract) {
      $form['xoxo']['status']['items'][] = '<strong>' . t("No backend set.") . '</strong>';
    }

    /*
     * FIXME:
     * 
     * This component handling (frontend) should be exported as a specific
     * form element available for everyone. It could be über usefull for
     * any other forms.
     * 
     * It would also capitalize inside this exact form by helping others
     * such as backend and formatter selection.
     */

    $form['frontend'] = array(
      '#type' => 'fieldset',
      '#title' => t("Frontend"),
      '#group' => 'components',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    // AJAX needed container.
    $form['frontend']['item']['#prefix'] = '<div id="frontend-wrapper">';
    $form['frontend']['item']['#suffix'] = '</div>';
    // AJAX multistep workflow.
    
    // Frontend specifics.
    $frontend = $parent->getFrontend(FALSE);
    if (isset($frontend)) {
      $form['frontend']['description'] = array(
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => t("Current frontend is <strong>@name</strong>.", array(
          '@name' => $frontend->getRegistry()->getItemName($frontend),
        )),
      );
      if ($frontend instanceof Formable_Interface) {
        $form['frontend']['item'] += array('#type' => 'oox_formable', '#object' => $frontend);
      }
      else {
        $form['frontend']['item'] += array('#markup' => '<p>' . t("This component has no configuration options.") . '</p>');
      }
    }
    $form['frontend']['item']['change'] = array(
      '#type' => 'submit',
      '#value' => isset($fontend) ? t("Change") : t("Select one"),
      '#submit' => 'vc_bundle_abstract_form_frontend_change_submit',
      '#ajax' => array(
        'callback' => 'vc_bundle_abstract_form_frontend_change_js',
        'wrapper' => 'frontend-wrapper',
      ),
    );

    /*
     * Ugly duplicated code, necessary though.
     */

    $form['formatter'] = array(
      '#type' => 'fieldset',
      '#title' => t("Formatter"),
      '#group' => 'components',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $formatter = $parent->getFormatter(FALSE);
    if (isset($formatter)) {
      $form['formatter']['description'] = array(
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => t("Current formatter is <strong>@name</strong>.", array(
          '@name' => $formatter->getRegistry()->getItemName($formatter),
        )),
      );      
      if ($formatter instanceof Formable_Interface) {
        $form['formatter']['item'] = array('#type' => 'oox_formable', '#object' => $formatter);
      }
      else {
        $form['formatter']['item'] = array('#prefix' => '<p>', '#suffix' => '</p>', '#markup' => t("This component has no configuration options.", array()));
      }
    }

    /*
     * Decorators handling.
     */

    $form['decorators'] = array(
      '#type' => 'fieldset',
      '#title' => t("Decorators"),
      '#group' => 'components',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['decorators']['decorators'] = array(
      '#prefix' => '<div class="clearfix" id="decorators-wrapper">',
      '#suffix' => '</div>',
    );
    $form['decorators']['decorators']['items'] = array('#theme' => 'vc_items_table');
    foreach ($parent->getDecorators() as $delta => $decorator) {
      $form['decorators']['decorators']['items'][$delta]['title'] = array(
        '#markup' => $decorator->getRegistry()->getItemName($decorator),
      );
      $form['decorators']['decorators']['items'][$delta]['weight'] = array(
        '#type' => 'hidden',
        '#default_value' => $delta,
      );
      $form['decorators']['decorators']['items'][$delta]['remove-' . $delta] = array(
        '#type' => 'submit',
        '#value' => t("Remove"),
        '#submit' => array('vc_bundle_abstract_form_decorators_remove_submit'),
        '#limit_validation_errors' => array(array('object_form', 'decorators')),
        '#attributes' => array('class' => array('use-ajax-submit')),
        '#ajax' => array(
          'callback' => 'vc_bundle_abstract_form_decorators_js',
          'wrapper' => 'decorators-wrapper',
        ),
      );
      if ($decorator instanceof Formable_Interface) {
        $form['decorators']['decorators']['items'][$delta]['item'] = array('#type' => 'oox_formable', '#object' => $decorator, '#dialog' => TRUE);
      }
    }
    $form['decorators']['decorators']['add'] = array(
      '#type' => 'oox_registry_item',
      '#title' => FALSE,
      '#registry_type' => 'vc_decorator',
      '#input_type' => 'select',
    );
    $form['decorators']['decorators']['add_submit'] = array(
      '#type' => 'submit',
      '#value' => t("Add"),
      '#submit' => array('vc_bundle_abstract_form_decorators_add_submit'),
      '#limit_validation_errors' => array(array('object_form', 'decorators')),
      '#attributes' => array('class' => array('use-ajax-submit')),
      '#ajax' => array(
        'callback' => 'vc_bundle_abstract_form_decorators_js',
        'wrapper' => 'decorators-wrapper',
      ),
    );

    $form['xoxo']['status'] = array('#markup' => theme('item_list', array('items' => $form['xoxo']['status']['items'])));
  }
}

/**
 * Add decorator submit handler.
 */
function vc_bundle_abstract_form_decorators_add_submit($form, &$form_state) {
  // FIXME: We now this is the path because it's written in the xoxo.admin.inc
  // file, but the Formable_Interface API should be path agnostic, and
  // should propose helpers to find out the real path, like by playing with the
  // form state array to set tags.
  $path = array('object_form');
  // FIXME: Once more, this is not written in the full pattern, we should
  // consider reworking this part.
  $object = $form_state['object'];
  // Get current select value.
  if ($type = $form_state['input']['object_form']['object']['decorators']['decorators']['add']['type']) {
    try {
      $item = oox_registry_get('vc_decorator')->getItem($type);
      $object->addDecorator($item);
    }
    catch (Oox_Exception $e) {
      // We should watchdog what happend here.
      die($e);
    }
  }
  // Mark the form for rebuild. The object will give the new decorator list to
  // build state.
  $form_state['rebuild'] = TRUE;
}

function vc_bundle_abstract_form_frontend_change_submit($form, &$form_state) {
  // FIXME: Do something.
}

function vc_bundle_abstract_form_frontend_change_js($form, $form_state) {
  return $form['object_form']['object']['frontend']['item'];
}

/**
 * Add decorator submit handler.
 */
function vc_bundle_abstract_form_decorators_remove_submit($form, &$form_state) {
  $object = $form_state['object'];
  // Fetch decorator delta to remove.
  // Proceed to variable recopy to avoid modifications.
  $parents = $form_state['clicked_button']['#parents'];
  list(, $delta) = explode('-', array_pop($parents), 2);
  $object->removeDecoratorAt($delta);
  // Mark the form for rebuild. The object will give the new decorator list to
  // build state.
  $form_state['rebuild'] = TRUE;
}

/**
 * Add decorator AJAX return callback.
 */
function vc_bundle_abstract_form_decorators_js($form, $form_state) {
  return $form['object_form']['object']['decorators']['decorators'];
}
