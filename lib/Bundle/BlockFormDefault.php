<?php

/**
 * Vc_Bundle_Block form helper.
 */
class Vc_Bundle_BlockFormDefault
  extends Vc_Bundle_AbstractFormDefault
{
  /**
   * @see Oox_Formable_Interface::formBuild()
   */
  public function formBuild(array &$form, array $values = array(), $parent = NULL) {
    parent::formBuild($form, $values, $parent);
    $form['basics']['title'] = array(
      '#title' => t("Title"),
      '#type' => 'textfield',
      '#default_value' => $parent->getTitle(FALSE),
      '#description' => t("This will be the block title. Object description will be used if none set.") . ' ' . t("Remember that you can disable the block title in the block administration pages."),
    );
    // Misc. options should go into this fieldset.
    $form['system'] = array(
      '#type' => 'fieldset',
      '#title' => t("System"),
      '#group' => 'components',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t("System options, this is the link between this block and core options."),
    );
    $form['system']['cacheMode'] = array(
      '#title' => t("Cache mode"),
      '#type' => 'select',
      '#options' => array(
        DRUPAL_CACHE_GLOBAL => t("Global"),
        DRUPAL_CACHE_PER_PAGE => t("Per page"),
        DRUPAL_CACHE_PER_ROLE => t("Per role"),
        DRUPAL_CACHE_PER_USER => t("Per user"),
        DRUPAL_CACHE_PER_USER | DRUPAL_CACHE_PER_PAGE => t("Per user and page"),
        DRUPAL_NO_CACHE => t("No cache"),
      ),
      '#default_value' => $parent->getCacheMode(),
      '#description' => t("Advanced users only.") . ' ' . t("Set Drupal cache mode. You can leave default, ") . ' ' . t("If you change this settings, you should visit the blocks administration page to ensure that these modification are taken into account."),
    );
  }

  /**
   * @see Oox_Formable_Interface::formSubmit()
   */
  public function formSubmit(array &$values, $parent = NULL) {
    parent::formSubmit($values, $parent);
    $parent->setTitle($values['basics']['title']);
    $parent->setCacheMode($values['system']['cacheMode']);
  }
}
