<?php

/**
 * Base implementation for Vc_Bundle_Interface.
 */
abstract class Vc_Bundle_Abstract
  extends Xoxo_Object_Exportable
  implements Vc_Bundle_Interface
{
  /**
   * @var ObjectStream_Interface
   */
  protected $_objectStream;

  /**
   * @see Vc_Bundle_Interface::setObjectStream()
   */
  public function setObjectStream(ObjectStream_Interface $objectStream) {
    $this->_objectStream = $objectStream;
    return $this;
  }

  /**
   * @see Vc_Bundle_Interface::getObjectStream()
   */
  public function getObjectStream($fallbackWithDummy = TRUE) {
    if (!isset($this->_objectStream) && $fallbackWithDummy) {
      // In case of any failure, use null object, we can't let the user face
      // a WSOD here (we are probably displaying it in the UI).
      return Oox_NullObject::getInstance('ObjectStream_Dummy');
    }
    return $this->_objectStream;
  }

  /**
   * @var Vc_Frontend_Interface
   */
  protected $_frontend;

  /**
   * @see Vc_Bundle_Interface::setFrontend()
   */
  public function setFrontend(Vc_Frontend_Interface $frontend) {
    $this->_frontend = $frontend;
    return $this;
  }

  /**
   * @see Vc_Bundle_Interface::getFrontend()
   */
  public function getFrontend($fallbackWithDummy = TRUE) {
    if (!isset($this->_frontend) && $fallbackWithDummy) {
      return Oox_NullObject::getInstance('Vc_Frontend_Dummy');
    }
    return $this->_frontend;
  }

  /**
   * @var Vc_Formatter_Interface
   */
  protected $_formatter;

  /**
   * @see Vc_Bundle_Interface::setFormatter()
   */
  public function setFormatter(Vc_Formatter_Interface $formatter) {
    $this->_formatter = $formatter;
    return $this;
  }

  /**
   * @see Vc_Bundle_Interface::getFormatter()
   */
  public function getFormatter($fallbackWithDummy = TRUE) {
    if (!isset($this->_formatter) && $fallbackWithDummy) {
      return Oox_NullObject::getInstance('Vc_Formatter_Dummy');
    }
    return $this->_formatter;
  }

  /**
   * @var array
   */
  protected $_decorators = array();

  /**
   * @see Vc_Bundle_Interface::removeDecorator()
   */
  public function removeDecorator(Vc_Decorator_Interface $decorator) {
    foreach ($this->_decorators as $key => $_decorator) {
      if ($decorator === $_decorator) {
        unset($this->_decorators[$key]);
      }
    }
    return $this;
  }

  /**
   * @see Vc_Bundle_Interface::removeDecoratorAt()
   */
  public function removeDecoratorAt($index) {
    if (isset($this->_decorators[$index])) {
      unset($this->_decorators[$index]);
    }
    return $this;
  }

  /**
   * @see Vc_Bundle_Interface::getDecorators()
   */
  public function getDecorators() {
    return $this->_decorators;
  }

  /**
   * @see Vc_Bundle_Interface::addDecorator()
   */
  public function addDecorator(Vc_Decorator_Interface $decorator) {
    foreach ($this->_decorators as $_decorator) {
      if ($decorator === $_decorator) {
        // Decorator already set here.
        return;
      }
    }
    $this->_decorators[] = $decorator;
    return $this;
  }

  /**
   * @see Formable_Interface::getForm()
   */
  public function getForm($formIdentifier = Formable_Interface::FORM_DEFAULT) {
    if (Formable_Interface::FORM_DEFAULT == $formIdentifier) {
      return new Vc_Bundle_AbstractFormDefault;
    }
  }
}
