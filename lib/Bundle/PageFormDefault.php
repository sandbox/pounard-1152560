<?php

/**
 * Vc_Bundle_Page form helper.
 */
class Vc_Bundle_PageFormDefault
  extends Vc_Bundle_AbstractFormDefault
{
  /**
   * @see Oox_Formable_Interface::formBuild()
   */
  public function formBuild(array &$form, array $values = array(), $parent = NULL) {
    parent::formBuild($form, $values, $parent);
    $form['basics']['path'] = array(
      '#title' => t("Path"),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $this->getPath(),
    );
    $form['basics']['title'] = array(
      '#title' => t("Title"),
      '#type' => 'textfield',
      // FIXME.
      // Do not use the accessor here, it would return the description field if
      // title is not already set. We must keep the form representative of the
      // real data set.
      '#default_value' => $this->getTitle(),
      '#description' => t("This will be the page title. Object description will be used if none set."),
    );
    $form['basics']['isLocalized'] = array(
      '#title' => t("Localized"),
      '#type' => 'checkbox',
      '#default_value' => $this->isLocalized(),
      '#disabled' => TRUE,
      '#description' => t("If checked, the page title must be written in Drupal default language (english). The title will be then exposed to translation using Drupal core interface translation administration page."),
    );
  }

  /**
   * @see Oox_Formable_Interface::formValidate()
   */
  public function formValidate(array &$values, $parent = NULL) {
    if (!$errors = parent::formValidate($values, $parent)) {
      $errors = array();
    }
    // FIXME: Should check at least for a valid path.
    return $errors;
  }

  /**
   * @see Oox_Formable_Interface::formSubmit()
   */
  public function formSubmit(array &$values, $parent = NULL) {
    parent::formSubmit($values, $parent);
    $parent->setPath($values['basics']['path']);
    $parent->setTitle($values['basics']['title']);
  }
}
