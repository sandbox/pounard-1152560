<?php

/**
 * A bundle is a pure configuration object. It connects together a request
 * and a frontend in a particular Drupal context.
 */
interface Vc_Bundle_Interface extends Vc_Component_Interface
{
  /**
   * Set request.
   * 
   * @param Vc_Backend_Interface $backend
   * 
   * @return Vc_Component_Interface
   *   Self reference for chaining.
   */
  public function setObjectStream(ObjectStream_Interface $stream);

  /**
   * Get current backend.
   * 
   * @param boolean $fallbackWithDummy = TRUE
   *   If set to TRUE, will give a dummy instance instead. This is likely to
   *   be used in pragmatic object initialization, notably when displaying it
   *   as a failsafe to ensure no crash for end users.
   * 
   * @return ObjectStream_Interface
   */
  public function getObjectStream($fallbackWithDummy = TRUE);

  /**
   * Set frontend.
   * 
   * @param Vc_Frontent_Interface $frontend
   */
  public function setFrontend(Vc_Frontend_Interface $frontend);

  /**
   * Get current frontend.
   * 
   * @param boolean $fallbackWithDummy = TRUE
   *   If set to TRUE, will give a dummy instance instead. This is likely to
   *   be used in pragmatic object initialization, notably when displaying it
   *   as a failsafe to ensure no crash for end users.
   * 
   * @return Vc_Frontend_Interface
   */
  public function getFrontend($fallbackWithDummy = TRUE);

  /**
   * Set formatter.
   * 
   * @param Vc_Formatter_Interface $formatter
   * 
   * @return Vc_Component_Interface
   *   Self reference for chaining.
   */
  public function setFormatter(Vc_Formatter_Interface $formatter);

  /**
   * Get current formatter.
   * 
   * @param boolean $fallbackWithDummy = TRUE
   *   If set to TRUE, will give a dummy instance instead. This is likely to
   *   be used in pragmatic object initialization, notably when displaying it
   *   as a failsafe to ensure no crash for end users.
   * 
   * @return Vc_Formatter_Interface
   */
  public function getFormatter($fallbackWithDummy = TRUE);

  /**
   * Remove given decorator.
   * 
   * @param Vc_Decorator_Interface $decorator
   * 
   * @return Vc_Bundle_Interface
   *   Self reference for chaining.
   */
  public function removeDecorator(Vc_Decorator_Interface $decorator);

  /**
   * Remove decorator at given position.
   * If index is out of range, this method will remain silent.
   * 
   * @param int $index
   * 
   * @return Vc_Bundle_Interface
   *   Self reference for chaining.
   */
  public function removeDecoratorAt($index);

  /**
   * Return all bundle decorators.
   * 
   * @return array
   *   Array of Vc_Decorator_Interface instances.
   */
  public function getDecorators();

  /**
   * Add filter.
   * 
   * @param Vc_Decorator_Interface $decorator
   * 
   * @return Vc_Bundle_Interface
   *   Self reference for chaining.
   */
  public function addDecorator(Vc_Decorator_Interface $decorator);
}
