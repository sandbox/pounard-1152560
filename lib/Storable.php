<?php

/**
 * Allows us to store arbitrary choosen backends into database.
 * 
 * This will allow only persistent backends to be stored. While manipulating
 * the API you may succeed in saving others, but results at load time will be
 * randomly reconstructed objects.
 */
class ObjectStream_Storable extends Xoxo_Object_Exportable
{
  protected $_stream;

  public function __construct() {
    parent::__construct($stream = NULL, $name = NULL, $description = NULL);
  }
}
