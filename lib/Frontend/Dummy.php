<?php

/**
 * Null Object pattern implementation.
 */
class Vc_Frontend_Dummy
  extends Vc_Component_Dummy
  implements Vc_Frontend_Interface
{
  /**
   * @see Vc_Frontend_Interface::render()
   */
  public function render(ObjectStream_Interface $objectStream, Vc_Formatter_Interface $formatter) {
    return '';
  }
}
