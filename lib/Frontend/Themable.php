<?php
 
/**
 * Base implementation of Vc_Frontend_Abstract uses a template for rendering.
 * This template can be overriden as you want.
 */
abstract class Vc_Frontend_Themable extends Vc_Component_Abstract
{
  /**
   * @see Vc_Frontend_Interface::render()
   */
  public function render(ObjectStream_Interface $objectStream, Vc_Formatter_Interface $formatter) {
    $build = array(
      '#theme' => 'vc_frontend',
      '#items' => array(),
    );
    $datatype = $objectStream->getDatatype();
    foreach ($objectStream as $object) {
      $build['#frontend'] = $this;
      $build['#datatype'] = $datatype;
      $build['#items'][] = $formatter->format($object, $datatype);
    }
    return $build;
  }
}
