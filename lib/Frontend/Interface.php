<?php

/**
 * A frontend is a plugin, along with its own configuration which is able to
 * display a request result set in the Drupal interface.
 * 
 * This frontend will use a request and a formatter together in order to
 * display content.
 */
interface Vc_Frontend_Interface extends Vc_Component_Interface
{
  /**
   * Render result using the request result set and the current option this
   * instance has. It can be called many time as you want, with different
   * options.
   * 
   * @param Oox_ObjectStream_Interface $objectStream
   *   Object stream.
   * @param Vc_Formatter_Interface $formatter
   *   Formatter to use.
   * 
   * @return string|array
   *   drupal_render() friendly structure.
   * 
   * @throws Vc_Exception
   *   If any error happen, a sample common error would be an attempt to render
   *   a result set the formatter can't render.
   */
  public function render(ObjectStream_Interface $objectStream, Vc_Formatter_Interface $formatter);
}
