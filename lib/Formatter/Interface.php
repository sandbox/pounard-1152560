<?php

/**
 * A formatter is a plugin able to display raw data from a result set. The
 * formatter is dependent on the data nature actually being manipulated.
 */
interface Vc_Formatter_Interface extends Vc_Component_Interface
{
  /**
   * Tells if the current formatter is able to manipulate data provided by the
   * given backend.
   * 
   * @param string $datatype
   *   Datatype this formatter can format.
   * 
   * @return boolean 
   */
  public function canFormat($datatype);

  /**
   * Format the given result. This result will always come from a known backend
   * this implementation can format.
   * 
   * @param mixed $object
   *   Unique piece of data coming back from a request using a known backend.
   * @param string $datatype
   *   Datatype the given data is.
   * 
   * @return array
   *   drupal_render() friendly structure..
   * 
   * @throws Vc_Exception
   *   If any error happen, a sample common error would be an attempt to render
   *   a result set this formatter can't render.
   */
  public function format($object, $datatype);
}
