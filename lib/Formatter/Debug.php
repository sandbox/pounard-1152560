<?php

/**
 * Formatter that you should not use unless you are a developer.
 */
class Vc_Formatter_Debug extends Vc_Component_Abstract
{
  /**
   * @see Vc_Formatter_Interface::canFormat()
   */
  public function canFormat($datatype) {
    return TRUE;
  }

  /**
   * @see Vc_Formatter_Interface::format()
   */
  public function format($object, $datatype) {
    return print_r($object, TRUE);
  }
}
