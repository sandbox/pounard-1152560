<?php

/**
 * Null Object pattern implementation.
 */
class Vc_Formatter_Dummy
  extends Vc_Component_Dummy
  implements Vc_Formatter_Interface
{
  /**
   * @see Vc_Formatter_Interface::canFormat()
   */
  public function canFormat($datatype) {
    return TRUE;
  }

  /**
   * @see Vc_Formatter_Interface::format()
   */
  public function format($object, $datatype) {
    return '';
  }
}
