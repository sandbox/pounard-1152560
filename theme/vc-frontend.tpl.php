<?php
// $Id$

/**
 * @file
 * Default frontend template implementation.
 */
?>
<div<?php print $attributes; ?>>
  <?php foreach ($items as $item): ?>
  <div><?php print render($item); ?></div>
  <?php endforeach; ?>
</div>