<?php
// $Id$

/**
 * @file
 * HTML list template implementation.
 */
?>
<ul<?php print $attributes; ?>>
  <?php foreach ($items as $item): ?>
  <li><?php print render($item); ?></li>
  <?php endforeach; ?>
</ul>