<?php
// $Id$

/**
 * @file
 * Default frontend template implementation.
 */
?>
<div<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <div<?php print $content_attributes; ?>>
    <?php print render($content); ?>
  </div>
</div>