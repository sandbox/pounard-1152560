<?php
// $Id$

/**
 * @file
 * View Composer theme functions.
 */

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_vc_frontend(&$variables) {
  $variables['items'] = $variables['element']['#items'];
  $variables['datatype'] = $variables['element']['#datatype'];

  // Do not allow any crash at rendering time.
  if (($frontend = $variables['element']['#frontend']) instanceof Vc_Frontend_Interface) {
    $variables['theme_hook_suggestions'][] = 'vc__frontend__' . $frontend->getType();
  }
}

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_vc_bundle(&$variables) {
  $bundle = $variables['element']['#bundle'];
  $decorators = $bundle->getDecorators();

  // Allow decorators to alter the bundle before the last execution.
  foreach ($decorators as $decorator) {
    $decorator->preQuery($bundle);
  }

  // Render content.
  $build = array();
  $build['bundle'] = $bundle->getFrontend()->render($bundle->getObjectStream(), $bundle->getFormatter());
  // Frontend render can be a either a string, either an elements array.
  if (!is_array($build['bundle'])) {
    $build['bundle'] = array('#markup' => $build['bundle']);
  }

  // Apply decorators.
  foreach ($decorators as $decorator) {
    $decorator->decorate($bundle, $build);
  }

  $variables['content'] = &$build;
}

/**
 * Theme an orderable item table sub-form.
 */
function theme_vc_items_table(&$variables) {
  $form = &$variables['form'];
  $rows = array();
  foreach (element_children($form) as $key) {
    $row = array();
    $row[] = array('data' => drupal_render($form[$key]['title']), 'class' => 'container-inline');
    $row[] = array('data' => drupal_render($form[$key]), 'class' => 'container-inline');
    $rows[] = $row;
  }
  return theme('table', array('rows' => $rows)) . drupal_render_children($form);
}
