<?php
// $Id$

/**
 * Interface for entity filter. Most of them should implement this interface.
 */
interface VcEntity_Filter_Interface extends Vc_Filter_Interface
{
  /**
   * Alter the given filter query.
   * 
   * @param VcEntity_Backend_Entity_Query $query
   */
  public function alterQuery(VcEntity_Backend_Entity_Query $query);

  /**
   * Tells if this implementation can be used for sorting results.
   * 
   * @return boolean
   */
  public function canSort();

  /**
   * Set datatype on which the current filter will query. This will be done
   * only at form build time, in order to build a restrictive and accurate
   * form.
   * 
   * Do not rely on this parameter in any way elsewhere, you won't need it
   * anyway since you should have set needed options at form submit time.
   * 
   * @param string $datatype
   */
  public function setDatatype($datatype);
}
