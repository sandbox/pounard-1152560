<?php
// $Id$

/**
 * Arbitrary entity field filter.
 */
class VcEntity_Filter_Field extends VcEntity_Filter_Abstract
{
  /**
   * @see VcEntity_Filter_Abstract::_alterSort()
   */
  protected function _alterSort(VcEntity_Backend_Entity_Query $query, $order) {
  }

  /**
   * @see VcEntity_Filter_Abstract::_alterFilter()
   */
  protected function _alterFilter(VcEntity_Backend_Entity_Query $query) {
  }

  /**
   * @see VcEntity_Filter_Interface::canSort()
   */
  public function canSort() {
    return TRUE;
  }

  /**
   * @see Vc_Filter_Interface::vary()
   */
  public function vary() {
    return FALSE;
  }

  /**
   * @see Vc_Filter_Interface::handles()
   */
  public function handles($datatype) {
    return TRUE;
  }

  /**
   * @see VcEntity_Filter_Abstract::formBuild()
   */
  public function formBuild(array &$form, array $values = array()) {
    parent::formBuild($form, $values);

    $info = entity_get_info($this->_datatype);
    $options = array();

    if ($info['fieldable']) {
      foreach (field_info_instances($this->_datatype) as $bundle => $fields) {
        foreach ($fields as $field_name => $field) {
          if (!$field['deleted']) {
            $field_info = field_info_field($field_name);
            $field_type = field_info_field_types($field_info['type']);
            $options[$field_name] = $field['label'] . " (" . $field_type['label'] . ")";
          }
        }
      }
    }

    $form['field'] = array(
      '#type' => 'select',
      '#title' => t("Select field"),
      '#options' => $options,
    );
  }

  /**
   * @see VcEntity_Filter_Abstract::formSubmit()
   */
  function formSubmit(array &$values) {
    parent::formSubmit($values);
  }
}
