<?php
// $Id$

/**
 * Date filter for entities.
 */
class VcEntity_Filter_Date extends VcEntity_Filter_Abstract
{
  /**
   * @see VcEntity_Filter_Abstract::_alterSort()
   */
  protected function _alterSort(VcEntity_Backend_Entity_Query $query, $order) {
    $query->propertyOrderBy('created', $order);
  }

  /**
   * @see VcEntity_Filter_Abstract::_alterFilter()
   */
  protected function _alterFilter(VcEntity_Backend_Entity_Query $query) {
    // FIXME: We need to create some sort of operator abstraction before.
  }

  /**
   * @see VcEntity_Filter_Interface::canSort()
   */
  public function canSort() {
    return TRUE;
  }

  /**
   * @see Vc_Filter_Interface::vary()
   */
  public function vary() {
    return FALSE;
  }

  /**
   * @see Vc_Filter_Interface::handles()
   */
  public function handles($datatype) {
    return TRUE;
  }
}
