<?php

/**
 * Base implementation for VcEntity_Filter_Interface
 */
abstract class VcEntity_Filter_Abstract extends Oox_Optionable implements VcEntity_Filter_Interface, Oox_Formable_Interface
{
  /**
   * @var string
   */
  protected $_datatype;

  /**
   * @see VcEntity_Filter_Interface::setDatatype()
   */
  public final function setDatatype($datatype) {
    $this->_datatype = $datatype;
  }

  /**
   * Alter the query using sort query.
   * 
   * Implement this only if your filter can sort.
   */
  protected function _alterSort(VcEntity_Backend_Entity_Query $query, $order) {}

  /**
   * Alter the query using this filter options.
   */
  protected abstract function _alterFilter(VcEntity_Backend_Entity_Query $query);

  /**
   * @see VcEntity_Filter_Interface::alterQuery()
   */
  public function alterQuery(VcEntity_Backend_Entity_Query $query) {
    switch ($this->getOption('use_as')) {
      case 'both':
        $this->_alterFilter($query);
        $this->_alterSort($query, $this->getOption('sort_order', 'asc'));
        break;

      case 'sort';
        $this->_alterSort($query, $this->getOption('sort_order', 'asc'));
        break;

      case 'filter':
      default:
        $this->_alterFilter($query);
        break;
    }
  }

  /**
   * @see Oox_Formable_Interface::formBuild()
   */
  public function formBuild(array &$form, array $values = array()) {
    if ($this->canSort()) {
      $form['use_as'] = array(
        '#title' => t("Use as"),
        '#type' => 'select',
        '#options' => array(
          'filter' => t("Filter"),
          'sort' => t("Sort"),
          'both' => t("Both"),
        ),
        '#default_value' => $this->getOption('use_as', 'asc'),
      );
      $form['sort_order'] = array(
        '#title' => t("Sort order to use, if sort enabled"),
        '#type' => 'select',
        '#options' => array(
          'asc' => t("Ascending"),
          'desc' => t("Descending"),
        ),
        '#default_value' => $this->getOption('sort_order', 'asc'),
      );
    }
    else {
      $form['mode'] = array('#type' => 'value', '#value' => 'filter');
    }
  }

  /**
   * @see Oox_Formable_Interface::formValidate()
   */
  public function formValidate(array &$values) {}

  /**
   * @see Oox_Formable_Interface::formSubmit()
   */
  public function formSubmit(array &$values) {
    $this->setOption('use_as', $values['use_as']);
    $this->setOption('sort_order', $values['sort_order']);
  }
}
