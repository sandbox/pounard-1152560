<?php
// $Id$

/**
 * Entity bundle filter.
 * 
 * Do not understand the 'bundle' word wrong here. It means, in this particular
 * class the 'Entity bundle' such as Drupal core defines it (and not a View
 * Composer backend).
 */
class VcEntity_Filter_EntityBundle extends VcEntity_Filter_Abstract
{
  /**
   * @see VcEntity_Filter_Abstract::_alterSort()
   */
  protected function _alterSort(VcEntity_Backend_Entity_Query $query, $order) {
    // This switch is best example of the entity API inconsistency. Why but
    // why do people are unable to do consistent code?
    switch ($query->entityConditions['entity_type']) {

      // Special case, comment can't be ordered using bundle.
      case 'comment':
        return;

      // Other special case, taxonomy term instances does not have bundles,
      // they only can be ordered using vocabulary tid.
      case 'taxonomy_term':
        $query->propertyOrderBy('vid', $order);
      	break;

      // Hope no other special cases are out there.
      default:
        $query->entityOrderBy('bundle', $order);
        break;
    }
  }

  /**
   * @see VcEntity_Filter_Abstract::_alterFilter()
   */
  protected function _alterFilter(VcEntity_Backend_Entity_Query $query) {
    if ($bundles = $this->getOption('bundles')) {
      $query->entityCondition('bundle', $bundles, 'IN');
    }
  }

  /**
   * @see VcEntity_Filter_Interface::canSort()
   */
  public function canSort() {
    return TRUE;
  }

  /**
   * @see Vc_Filter_Interface::vary()
   */
  public function vary() {
    return FALSE;
  }

  /**
   * @see Vc_Filter_Interface::handles()
   */
  public function handles($datatype) {
    return TRUE;
  }

  /**
   * @see VcEntity_Filter_Abstract::formBuild()
   */
  public function formBuild(array &$form, array $values = array()) {
    parent::formBuild($form, $values);

    $options = array();
    $info = entity_get_info($this->_datatype);
    foreach ($info['bundles'] as $bundle => $data) {
      $options[$bundle] = $data['label']; 
    }

    $form['bundles'] = array(
      '#type' => 'checkboxes',
      '#title' => t("Bundles"),
      '#options' => $options,
      '#default_value' => $this->getOption('bundles', array())
    );
  }

  /**
   * @see VcEntity_Filter_Abstract::formSubmit()
   */
  function formSubmit(array &$values) {
    parent::formSubmit($values);

    $bundles = array();
    foreach ($values['bundles'] as $bundle => $enabled) {
      if ($enabled) {
        $bundles[$bundle] = $bundle;
      }
    }
    $this->setOption('bundles', $bundles);
  }
}
