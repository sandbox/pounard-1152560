<?php
// $Id$

/**
 * Display a node using a configurable view mode.
 */
class VcEntity_Formatter_Node extends Vc_Formatter_Abstract implements Oox_Formable_Interface
{
  /**
   * Get view mode.
   * 
   * @return string
   */
  public function getViewMode() {
    if (isset($this->_options['view_mode'])) {
      return $this->_options['view_mode'];
    }
    // We have at least to have a default here.
    return 'teaser';
  }

  /**
   * @see Vc_Formatter_Interface::canFormat()
   */
  public function canFormat($datatype) {
    return $datatype == 'node';
  }

  /**
   * @see Vc_Formatter_Interface::format()
   */
  public function format($object, $datatype) {
    return node_view($object, $this->getViewMode());
  }

  /**
   * @see Oox_Formable_Interface::formBuild()
   */
  public function formBuild(array &$form, array $values = array()) {
    $form = array();

    $entity_info = entity_get_info('node');
    $options = array();
/*    foreach () {
      
    }*/

    $form['view_mode'] = array(
      '#type' => 'radios',
      '#title' => t("Display mode"),
      '#options' => $options,
      '#required' => TRUE,
      '#default_value' => $this->getViewMode(),
    );
  }

  /**
   * @see Oox_Formable_Interface::formValidate()
   */
  public function formValidate(array &$values) {}

  /**
   * @see Oox_Formable_Interface::formSubmit()
   */
  public function formSubmit(array &$values) {
    $this->setOption('view_mode', $values['view_mode']);
  }
}
