<?php
// $Id$

/**
 * Display a node using a configurable view mode.
 */
class VcEntity_Formatter_NodeTitle extends Vc_Formatter_Abstract
{
  /**
   * @see Vc_Formatter_Interface::canFormat()
   */
  public function canFormat($datatype) {
    return $datatype == 'node';
  }

  /**
   * @see Vc_Formatter_Interface::format()
   */
  public function format($object, $datatype) {
    return l($object->title, 'node/' . $object->nid);
  }
}
