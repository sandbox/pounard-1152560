<?php
// $Id$

/**
 * Display a node using a configurable view mode.
 */
class VcEntity_Formatter_User extends Vc_Formatter_Abstract
{
  /**
   * @see Vc_Formatter_Interface::canFormat()
   */
  public function canFormat($datatype) {
    return $datatype == 'user';
  }

  /**
   * @see Vc_Formatter_Interface::format()
   */
  public function format($object, $datatype) {
    return theme('username', array('account' => $object));
  }
}
