<?php
// $Id$

/**
 * @file
 * View Composer UI admin pages.
 */

/**
 * Bundle creation.
 * Multistep form implementation.
 */
function vc_ui_bundle_wizard_form(&$form, &$form_state) {
  $form_state['storage']['steps'] = array(
  	array('vc_ui_bundle_wizard_step_backend', t("Request")),
    array('vc_ui_bundle_wizard_step_formatter', t("Formatter")),
    array('vc_ui_bundle_wizard_step_frontend', t("Frontend")),
    array('vc_ui_bundle_wizard_step_bundle', t("Bundle type")),
  	array('vc_ui_bundle_wizard_step_basic', t("Basic information")),
  );
  $form_state['storage']['redirect'] = 'admin/structure/vc';
  return vc_ui_wizard_build(&$form, &$form_state);
}

function vc_ui_bundle_backend_description($backend) {
  return check_plain($backend->getDescription()) . ' <small>(<em>' . check_plain($backend->getName()) . '</em>)</small>';
}

function vc_ui_bundle_wizard_step_backend(&$form, &$form_state) {
  $form['foo'] = array(
    '#prefix' => '<p>',
    '#suffix' => '</p>',
    '#markup' => t("If you are unable to find the request you want, you may want to proceed to <a href=\"!url\">request creation wizard</a> first.", array(
      '!url' => url('admin/structure/vc/request-wizard', array('query' => array('destination' => $_GET['q']))),
    )),
  );
  $options = array();
  $storage = xoxo_storage_get('vc_backend');
  $identifiers = $storage
    ->getQuery()
    ->execute()
    ->fetchCol();
  // Build menu items.
  foreach ($storage->load($identifiers) as $backend) {
    $options[$backend->getIdentifier()] = vc_ui_bundle_backend_description($backend);
  }
  $form['backend'] = array(
    '#type' => 'radios',
    '#options' => $options,
    '#description' => t("Select the request you want to display."),
    '#required' => TRUE,
  );
  return $form;
}

function vc_ui_bundle_wizard_step_backend_submit($form, &$form_state) {
  $oid = $form_state['storage']['backend'] = $form_state['values']['backend'];
  $backend = xoxo_storage_get('vc_backend')->loadByIdentifier($oid);
  $form_state['storage']['status'] = t("Request is is <em>!description</em>", array(
    '!description' => vc_ui_bundle_backend_description($backend),
  ));
}

function vc_ui_bundle_wizard_step_formatter(&$form, &$form_state) {
  $backend = xoxo_storage_get('vc_backend')->loadByIdentifier($form_state['storage']['backend']);
  $form['formatter'] = array(
    '#title' => t("Select formatter"),
    '#type' => 'oox_registry_item',
    '#registry_type' => 'vc_formatter',
    '#registry_cb' => 'getPotentialItemsFor',
    '#registry_cb_args' => array($backend->getDatatype()),
    '#required' => TRUE,
  );
  return $form;
}

function vc_ui_bundle_wizard_step_formatter_submit($form, &$form_state) {
  $formatter_type = $form_state['storage']['formatter'] = $form_state['values']['formatter'];
  $formatter_item = oox_registry_get('vc_formatter')->getItemCache($formatter_type);
  $form_state['storage']['status'] = t("Formatter is <em>@name</em>", array(
    '@name' => t($formatter_item['name']),
  )); 
}

function vc_ui_bundle_wizard_step_frontend(&$form, &$form_state) {
  $options = array();
  foreach (oox_registry_get('vc_frontend')->getItemCache() as $type => $item) {
    $options[$type] = vc_ui_format_item_radio($item);
  }
  $form['frontend'] = array(
    '#title' => t("Select frontend"),
    '#type' => 'radios',
    '#options' => $options,
    '#required' => TRUE,
    '#description' => t("Frontend is the request decoration, it will determine the bundle final HTML layout."),
  );
  return $form;
}

function vc_ui_bundle_wizard_step_frontend_submit($form, &$form_state) {
  $frontend_type = $form_state['storage']['frontend'] = $form_state['values']['frontend'];
  $frontend_item = oox_registry_get('vc_frontend')->getItemCache($frontend_type);
  $form_state['storage']['status'] = t("Frontend is <em>@name</em>", array(
    '@name' => t($frontend_item['name']),
  )); 
}

function vc_ui_bundle_wizard_step_bundle(&$form, &$form_state) {
  $options = array();
  foreach (oox_registry_get('vc_bundle')->getItemCache() as $type => $item) {
    $options[$type] = vc_ui_format_item_radio($item);
  }
  $form['bundle'] = array(
    '#title' => t("Select bundle type"),
    '#type' => 'radios',
    '#options' => $options,
    '#required' => TRUE,
    '#description' => t("Bundle type will determine how and when to display the bundle frontend."),
  );
  return $form;
}

function vc_ui_bundle_wizard_step_bundle_submit($form, &$form_state) {
  $bundle_type = $form_state['storage']['bundle'] = $form_state['values']['bundle'];
  $bundle_item = oox_registry_get('vc_bundle')->getItemCache($bundle_type);
  $form_state['storage']['status'] = t("Bundle is <em>@name</em>", array(
    '@name' => t($bundle_item['name']),
  )); 
}

function vc_ui_bundle_wizard_step_basic(&$form, &$form_state) {
  // FIXME: This is the ugly part, we c/c'ed the xoxo edit form to get those two
  // parameters. These should be in their own form, invoked here.
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t("Name"),
    '#description' => t("This is the internal name. Only only alpha-numerical characters, and '_' or '-' are allowed."),
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t("Description"),
    '#description' => t("Short description about this object."),
    '#default_value' => '',
  );
  return $form;
}

function vc_ui_bundle_wizard_step_basic_validate(&$form, &$form_state) {
  if (isset($form_state['values']['name'])) {
    if (!preg_match('/^[a-zA-Z0-9_-]+$/', trim($form_state['values']['name']))) {
      form_set_error('name', t("Title contains illegal characters."));
    }
  }
}

/**
 * Bundle creation.
 * d) Bundle basic options (XoXo object form).
 */
function vc_ui_bundle_wizard_step_basic_submit(&$form, &$form_state) {
  try {
    // Create new backend instance.
    $bundle = oox_registry_get('vc_bundle')->getItem($form_state['storage']['bundle']);
    $bundle->setName($form_state['values']['name']);
    $bundle->setDescription($form_state['values']['description']);

    // Set the backend.
    $backend = xoxo_storage_get('vc_backend')->loadByIdentifier($form_state['storage']['backend']);
    $bundle->setBackend($backend);

    // Fetch dependencies, and saves them with the new bundle.
    $formatter = oox_registry_get('vc_formatter')->getItem($form_state['storage']['formatter']);
    $frontend = oox_registry_get('vc_frontend')->getItem($form_state['storage']['frontend']);
    $bundle->setFrontend($frontend);
    $bundle->setFormatter($formatter);

    $storage = xoxo_storage_get('vc_bundle');

    // Save it.
    switch ($storage->save($bundle)) {
      case Xoxo_Storage::SAVED_ERROR:
        drupal_set_message(t("An error happened while saving the new bundle, contact the site administrator."), 'error');
        return;
        break;

      case Xoxo_Storage::SAVED_NEW:
        drupal_set_message(t("New bundle <em>@description</em> has been saved.", array(
          '@description' => $bundle->getDescription(),
        )));
        break;

      case Xoxo_Storage::SAVED_UPDATED:
        drupal_set_message(t("New bundle <em>@description</em> has been updated.", array(
          '@description' => $bundle->getDescription(),
        )));
        break;
    }

    $form_state['redirect'] = $storage->getPath() . '/' . $bundle->getName() . '/edit';
  }
  catch (Vc_Exception $e) {
    watchdog_exception('vc_ui', $e);
    drupal_set_message(t("An error happened while saving the new bundle, contact the site administrator."), 'error');
  }
  catch (Oox_Exception $e) {
    watchdog_exception('vc_ui', $e);
    drupal_set_message(t("An error happened while saving the new bundle, contact the site administrator."), 'error');
  }
}
