<?php
// $Id$

/**
 * @file
 * View Composer UI backend wizard.
 */

/**
 * Request creation.
 * Multistep form implementation.
 */
function vc_ui_request_wizard_form(&$form, &$form_state) {
  $form_state['storage']['steps'] = array(
    array('vc_ui_request_wizard_step_a', t("Datatype")),
    array('vc_ui_request_wizard_step_b', t("Backend")),
    array('vc_ui_request_wizard_step_c', t("Basic information")),
  );
  $form_state['storage']['redirect'] = 'admin/structure/vc';
  return vc_ui_wizard_build(&$form, &$form_state);
}

/**
 * Request creation.
 * a) Datatype selection.
 */
function vc_ui_request_wizard_step_a(&$form, &$form_state) {
  $options = array();
  foreach (vc_supported_datatypes() as $datatype => $description) {
    $options[$datatype] = vc_ui_format_item_radio($description);
  }
  $form['datatype'] = array(
    '#type' => 'radios',
    '#options' => $options,
    '#description' => t("Select the datatype you want to list."),
  );
  return $form;
}

function vc_ui_request_wizard_step_a_submit($form, &$form_state) {
  $form_state['storage']['datatype'] = $form_state['values']['datatype'];
  $types = vc_supported_datatypes();
  $form_state['storage']['status'] = t("Datatype is <em>@type</em>", array(
    '@type' => t($types[$form_state['values']['datatype']]['name']),
  )); 
}

/**
 * Request creation.
 * b) Backend selection.
 */
function vc_ui_request_wizard_step_b(&$form, &$form_state) {
  $datatype = $form_state['storage']['datatype'];
  $form['backend'] = array(
    '#title' => t("Select backend"),
    '#type' => 'oox_registry_item',
    '#registry_type' => 'vc_backend',
    '#registry_cb' => 'getPotentialItemsFor',
    '#registry_cb_args' => array($datatype),
    '#required' => TRUE,
  );
  return $form;
}

function vc_ui_request_wizard_step_b_submit($form, &$form_state) {
  $form_state['storage']['backend'] = $form_state['values']['backend'];
  $item = oox_registry_get('vc_backend')->getItemCache($form_state['storage']['backend']);
  $form_state['storage']['status'] = t("Backend is <em>@type</em>", array(
    '@type' => t($item['name']),
  )); 
}

/**
 * Request creation.
 * c) Backend basic options (XoXo object form).
 */
function vc_ui_request_wizard_step_c(&$form, &$form_state) {
  // FIXME: This is the ugly part, we c/c'ed the xoxo edit form to get those two
  // parameters. These should be in their own form, invoked here.
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t("Name"),
    '#description' => t("This is the internal name. Only only alpha-numerical characters, and '_' or '-' are allowed."),
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t("Description"),
    '#description' => t("Short description about this object."),
    '#default_value' => '',
  );
  return $form;
}

function vc_ui_request_wizard_step_c_validate(&$form, &$form_state) {
  if (isset($form_state['values']['name'])) {
    if (!preg_match('/^[a-zA-Z0-9_-]+$/', trim($form_state['values']['name']))) {
      form_set_error('name', t("Title contains illegal characters."));
    }
  }
}

function vc_ui_request_wizard_step_c_submit(&$form, &$form_state) {
  try {
    // Create new backend instance.
    $backend = oox_registry_get('vc_backend')->getItem($form_state['storage']['backend']);
    $backend->setName($form_state['values']['name']);
    $backend->setDescription($form_state['values']['description']);
    $backend->setDatatype($form_state['storage']['datatype']);

    $storage = xoxo_storage_get('vc_backend');

    // Save it.
    switch ($storage->save($backend)) {
      case Xoxo_Storage::SAVED_ERROR:
        drupal_set_message(t("An error happened while saving the new request, contact the site administrator."), 'error');
        return;
        break;

      case Xoxo_Storage::SAVED_NEW:
        drupal_set_message(t("New request <em>@description</em> has been saved.", array(
          '@description' => $backend->getDescription(),
        )));
        break;

      case Xoxo_Storage::SAVED_UPDATED:
        drupal_set_message(t("New request <em>@description</em> has been updated.", array(
          '@description' => $backend->getDescription(),
        )));
        break;
    }

    $form_state['redirect'] = $storage->getPath() . '/' . $backend->getName() . '/edit';
  }
  catch (Vc_Exception $e) {
    watchdog_exception('vc_ui', $e);
    drupal_set_message(t("An error happened while saving the new request, contact the site administrator."), 'error');
  }
  catch (Oox_Exception $e) {
    watchdog_exception('vc_ui', $e);
    drupal_set_message(t("An error happened while saving the new request, contact the site administrator."), 'error');
  }
}
