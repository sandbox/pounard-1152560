<?php
// $Id$

/**
 * @file
 * View Composer UI wizard template.
 */
?>
<div class="vc-wizard-form" style="position: relative">
  <div class="vc-wizard-form-left" style="position: absolute; top: 0; left: 0; width: 200px;">
    <?php hide($form['header_status']); ?>
    <?php print render($form['header_status']); ?>
  </div>
  <div class="vc-wizard-form-right" style="margin-left: 200px;">
    <?php print drupal_render_children($form); ?>
  </div>
</div>