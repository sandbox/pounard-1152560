<?php
// $Id$

/**
 * @file
 * View Composer UI theme functions.
 */

/**
 * Implements template_preprocess_TEMPLATE().
 */
function template_preprocess_vc_ui_wizard(&$variables) {
}

/**
 * Copy of theme_task_list() function, see http://drupal.org/node/1007564
 */
function theme_vc_task_list($variables) {
  $items = $variables['items'];
  $active = $variables['active'];
  $output = '';

  $done = isset($items[$active]) || $active == NULL;
  if (isset($variables['title'])) {
    $output .= '<h2 class="element-invisible">' . $variables['title'] . '</h2>';
  }
  $output .= '<ol class="task-list">';

  foreach ($items as $k => $item) {
    if ($active == $k) {
      $class = 'active';
      $status = '(' . t('active') . ')';
      $done = FALSE;
    }
    else {
      $class = $done ? 'done' : '';
      $status = $done ? '(' . t('done') . ')' : '';
    }
    $output .= '<li';
    $output .= ($class ? ' class="' . $class . '"' : '') . '>';
    $output .= $item;
    $output .= ($status ? '<span class="element-invisible">' . $status . '</span>' : '');
    $output .= '</li>';
  }
  $output .= '</ol>';
  return $output;
}
