<?php
// $Id$

/**
 * Display a node using a configurable view mode.
 */
class VcXoxo_Formatter_Description extends Vc_Formatter_Abstract
{
  /**
   * @see Vc_Formatter_Interface::canFormat()
   */
  public function canFormat($datatype) {
    return substr($datatype, 0, 5) == 'xoxo_';
  }

  /**
   * @see Vc_Formatter_Interface::format()
   */
  public function format($object, $datatype) {
    $storage = $object->getStorage();
    $path = $storage->getPath() . '/' . ($object instanceof Xoxo_Object_Exportable ? $object->getName() : $object->getIdentifier());
    return l($object->getDescription(), $path);
  }
}
