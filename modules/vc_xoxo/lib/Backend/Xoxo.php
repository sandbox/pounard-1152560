<?php
// $Id$

/**
 * Xoxo objects backend for VC module.
 */
class VcXoxo_Backend_Xoxo extends Vc_Backend_Persistent_Browsable
{
  /**
   * Get associated storage with datatype.
   * 
   * @param string $datatype
   * 
   * @return Xoxo_Storage
   */
  protected function _getStorage($datatype) {
    list(, $storage_type) = explode('_', $datatype, 2);
    return xoxo_storage_get($storage_type);
  }

  /**
   * @see Vc_Backend_Interface::canFetch()
   */
  public function canFetch($datatype) {
    return $this->_getStorage($datatype) instanceof Xoxo_Storage;
  }

  /**
   * Fetched objects.
   * 
   * @var array
   */
  protected $_objects;

  /**
   * Objects count.
   * 
   * @var int
   */
  protected $_count = 0;

  /**
   * Current position in objects.
   * 
   * @var int
   */
  protected $_pos = 0;

  /**
   * Is our property object modified?
   * 
   * @var boolean
   */
  protected $_modified = FALSE;

  /**
   * Do the actual query (internal).
   */
  protected function _query() {
    if ($this->_modified || !isset($this->_objects)) {
      // Clean up internals.
      $this->_objects = array();
      $this->_modified = FALSE;

      $conditions = array();

      // Fetch XoXo Storage.
      $storage = $this->_getStorage($this->_options['datatype']);
      $select = $storage->getQuery();

      // Call filters alteration.
      foreach ($this->_options['filters'] as $filter) {
        $filter->alterConditions($select);
      }

      // Do a count query before.
      $count_select = $select->countQuery()->execute()->fetchField();

      // Do the real select. Needs at least a limit to do this. '0' limits will
      // make the DBTNG layer fail.
      if ($this->_options['limit']) {
        $select->range($this->_options['offset'], $this->_options['limit']);
      }

      // Fetch the real objects.
      $oids = $select->execute()->fetchCol();
      $this->_objects = $storage->load($oids);
    }
  }

  /**
   * @see Countable::count()
   */
  public function count() {
    $this->_query();
    return $this->_count;
  }

  /**
   * Query the Drupal entity storage and fetch entities.
   * 
   * @return array
   *   Array of entities. Type of returned depend on the entity type which has
   *   been set as the datatype parameter.
   */
  public function query() {
    $this->_query();
    return $this;
  }

  /**
   * @see Iterator::current()
   */
  public function current() {
    if (!isset($this->_objects)) {
      throw new Vc_Exception("Query did not run yet.");
    }
    return current($this->_objects);
  }

  /**
   * @see Iterator::key()
   */
  public function key() {
    if (!isset($this->_objects)) {
      throw new Vc_Exception("Query did not run yet.");
    }
    return key($this->_objects);
  }

  /**
   * @see Iterator::next()
   */
  public function next() {
    if (!isset($this->_objects)) {
      throw new Vc_Exception("Query did not run yet.");
    }
    next($this->_objects);
    return $this->_pos++;
  }

  /**
   * @see Iterator::rewind()
   */
  public function rewind() {
    if (!isset($this->_objects)) {
      $this->query();
    }
    reset($this->_objects);
    $this->_pos = 0;
  }

  /**
   * @see Iterator::valid()
   */
  public function valid() {
    if (!isset($this->_objects)) {
      throw new Vc_Exception("Query did not run yet.");
    }
    return $this->_pos < count($this->_objects);
  }

  /**
   * @see Vc_Backend_Interface::getFilterInterface()
   */
  public function getFilterInterface() {
    return 'IVcEntityFilterCondition';
  }
}