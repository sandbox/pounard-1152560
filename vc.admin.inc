<?php
// $Id$

/**
 * @file
 * View Composer admin pages.
 */

/**
 * Backend preview page.
 */
function vc_admin_backend_preview_page(Vc_Backend_Interface $backend) {
  $item = vc_supported_datatypes($backend->getDatatype());

  // Use the raw frontend here for listing. We don't need fancy display.
  $frontend = oox_registry_get('vc_frontend')->getItem('raw');
  // Use the given default formatter.
  $formatter = oox_registry_get('vc_formatter')->getItem(isset($item['default_formatter']) ? $item['default_formatter'] : 'debug');
  // Display only 10 items.
  $backend->setLimit(10);

  // Create a bundle, add some technical information arround.
  $bundle = Oox_Registry::getInstance('vc_bundle')->getItem('page');
  $bundle->setFrontend($frontend);
  $bundle->setFormatter($formatter);
  $bundle->setObjectStream($backend);
  // Add the profiling and pager decorators, so the user can check performances
  // and navigate through results.
  $bundle->addDecorator(Oox_Registry::getInstance('vc_decorator')->getItem('profiling'));
  $bundle->addDecorator(Oox_Registry::getInstance('vc_decorator')->getItem('pager'));

  // Finally, display bundle.
  return array(
    '#theme' => 'vc_bundle',
    '#bundle' => $bundle,
  );
}

/**
 * Bundle preview page.
 */
function vc_admin_bundle_preview_page(Vc_Bundle_Interface $bundle) {
  // Append the profiling timer.
  $found = FALSE;
  foreach ($bundle->getDecorators() as $decorator) {
    if ($decorator instanceof Vc_Decorator_Profiling) {
      $found = TRUE;
      break;
    }
  }
  if (!$found) {
    $bundle->addDecorator(Oox_Registry::getInstance('vc_decorator')->getItem('profiling'));
  }

  // Finally, display bundle.
  return array(
    '#theme' => 'vc_bundle',
    '#bundle' => $bundle,
  );
}
